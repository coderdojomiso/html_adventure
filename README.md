# HTML Adventure

This is a tutorial on HTML which uses a gamebook-style adventure to teach some basic HTML components and how to create headers, links, lists.

You may install it with the `install.sh` file, which copies all files in the given directory, replacing some placeholders into the HTML files, such as the installation directory, so that instructions are given with the exact paths.

Installation also creates a bookmarks.html files in Netscape format, which can be easily imported into Firefox. The resulting menu will contain all the pages that the user will create/edit during the tutorial.