#!/bin/bash

version=$(date +%Y%m%d%H%M%S)

git flow release start ${version}
echo ${version} > version.txt
git add version.txt
git ci -m "Version bumped"
git flow release finish -p -m "Version ${version}"
