#!/bin/bash

if [[ -z ${1} ]]; then echo "$0 <install_directory>"; exit 1; fi

version=$(cat version.txt)

install_dir=${1}

if [[ ! -d ${install_dir} ]]; then
    echo "Creating directory ${install_dir}..."
    mkdir ${install_dir}
fi

if [[ -d ${install_dir} ]]; then
    rm -fR ${install_dir}
fi

echo "Creating directory ${install_dir}..."
mkdir ${install_dir}

echo
echo "Copying tutorial files..."
for i in "tutorial" "part1" "part2" "part3"; do
    cp -R ${i} ${install_dir}
done

echo
echo "Adjusting tutorial files to reflect the local installation..."
for i in $(find ${install_dir} -iname "*.html"); do
    echo "  * Processing ${i}"
    cat ${i} | sed -r -e s,"##base_dir##","${install_dir}", -e s,"##version##","${version}", > ${i}.tmp
    mv ${i}.tmp ${i}
done

echo
echo "Creating bookmarks file..."
bookmarks_file=${install_dir}/bookmarks.html

touch ${bookmarks_file}

echo "<!DOCTYPE NETSCAPE-Bookmark-file-1>" >> ${bookmarks_file}
echo "<!-- This is an automatically generated file." >> ${bookmarks_file}
echo "     It will be read and overwritten." >> ${bookmarks_file}
echo "     DO NOT EDIT! -->" >> ${bookmarks_file}
echo "<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=UTF-8\">" >> ${bookmarks_file}
echo "<TITLE>CoderDojo Bookmarks</TITLE>" >> ${bookmarks_file}
echo "<H1>CoderDojo Bookmarks</H1>" >> ${bookmarks_file}
echo "" >> ${bookmarks_file}
echo "<DL><p>" >> ${bookmarks_file}
echo "<DT><H3>HTML Adventure</H3>" >> ${bookmarks_file}
echo "    <DL><p>" >> ${bookmarks_file}

echo "        <DT><A HREF=\"file://${install_dir}/tutorial/index.html\">Tutorial</A>" >> ${bookmarks_file}
echo "        <DD>Il tutorial HTML" >> ${bookmarks_file}

echo "        <DT><A HREF=\"file://${install_dir}/part1/index.html\">Parte 1</A>" >> ${bookmarks_file}
echo "        <DD>La prima parte" >> ${bookmarks_file}

echo "        <DT><A HREF=\"file://${install_dir}/part2/01.html\">Parte 2</A>" >> ${bookmarks_file}
echo "        <DD>La seconda parte" >> ${bookmarks_file}

echo "        <DT><A HREF=\"file://${install_dir}/part3/fantasy/01.html\">Parte 3 - fantasy</A>" >> ${bookmarks_file}
echo "        <DD>La terza parte" >> ${bookmarks_file}

echo "        <DT><A HREF=\"file://${install_dir}/part3/scifi/01.html\">Parte 3 - fantascienza</A>" >> ${bookmarks_file}
echo "        <DD>La terza parte" >> ${bookmarks_file}

echo "        <DT><A HREF=\"file://${install_dir}/part3/adventure/01.html\">Parte 3 - avventura</A>" >> ${bookmarks_file}
echo "        <DD>La terza parte" >> ${bookmarks_file}

echo "    </DL><p>" >> ${bookmarks_file}
echo "</DL>" >> ${bookmarks_file}

echo
echo "Installation succesful"
echo "You will find everything in the directory ${install_dir}"
echo "IMPORT in Firefox the bookmarks file ${bookmarks_file} and move the imported directory into the bookmark bar."
echo